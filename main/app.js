function distance(first, second){
	if(!Array.isArray(first) || !Array.isArray(second)) {
		throw new TypeError("InvalidType");
	}
	var first1 = first.filter(function(a) {return second.indexOf(a) < 0});
	var second1 = second.filter(function(a) {return first.indexOf(a) < 0});
	first1 = [...new Set(first1)];
	second1 = [...new Set(second1)];
	var dist = first1.length + second1.length;
	return dist;
}

module.exports.distance = distance